package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.ppd.zapzap.ChatClientImpl;

public class BatePapoGUI extends JFrame {

	private JPanel contentPane;
	private JTextField tfEnviarMessage;
	private String amigoBatePapo;
	private JLabel lbAmigoBatePapo;
	private ChatClientImpl chatClientImpl;
	private String usuarioLocal;
	private JTextArea taMessage; 
	
	
	public static void main(String[] args) {
		try {					
			BatePapoGUI frame = new BatePapoGUI();
			frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the frame.
	 */
	public BatePapoGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 352, 444);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		taMessage = new JTextArea();
		taMessage.setEditable(false);
		taMessage.setBounds(5, 26, 328, 307);
		contentPane.add(taMessage);
		
		JLabel lblAmigo = new JLabel("Amigo:");
		lblAmigo.setBounds(12, 0, 55, 27);
		contentPane.add(lblAmigo);
		
		lbAmigoBatePapo = new JLabel(amigoBatePapo);
		lbAmigoBatePapo.setBounds(74, 6, 259, 15);
		contentPane.add(lbAmigoBatePapo);
		
		tfEnviarMessage = new JTextField();
		tfEnviarMessage.setBounds(5, 345, 229, 63);
		contentPane.add(tfEnviarMessage);
		tfEnviarMessage.setColumns(10);
		
		JButton btnNewButton = new JButton("Enviar");
		btnNewButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String message = tfEnviarMessage.getText();				
				chatClientImpl.enviarMessagem(usuarioLocal, amigoBatePapo, message);
			}
		});
		btnNewButton.setBounds(246, 345, 87, 25);
		contentPane.add(btnNewButton);
	}
    public void setAmigoBatePapo(String amigoBatePapo){
    	this.lbAmigoBatePapo.setText(amigoBatePapo);
    	this.amigoBatePapo = amigoBatePapo;
    	
    }
    
    public ChatClientImpl getChatClientImpl() {
		return chatClientImpl;
	}

	public void setChatClientImpl(ChatClientImpl chatClientImpl) {
		this.chatClientImpl = chatClientImpl;
	}

	public String getUsuarioLocal() {
		return usuarioLocal;
	}

	public void setUsuarioLocal(String usuarioLocal) {
		this.usuarioLocal = usuarioLocal;
	}
	
	

	public JLabel getLbAmigoBatePapo() {
		return lbAmigoBatePapo;
	}

	public void setLbAmigoBatePapo(JLabel lbAmigoBatePapo) {
		this.lbAmigoBatePapo = lbAmigoBatePapo;
	}

	public JTextArea getTaMessage() {
		return taMessage;
	}

	public void setTaMessage(JTextArea taMessage) {
		this.taMessage = taMessage;
	}

	public JTextField getTfEnviarMessage() {
		return tfEnviarMessage;
	}

	public void setTfEnviarMessage(JTextField tfEnviarMessage) {
		this.tfEnviarMessage = tfEnviarMessage;
	}	
   
}
