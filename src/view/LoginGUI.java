package view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;
import org.ppd.zapzap.ChatClientImpl;

import chat.ChatClient;
import chat.ChatServer;
import chat.ChatServerHelper;

public class LoginGUI extends JFrame {

	private JPanel contentPane;
	private JTextField logintf;
	private static ChatClientImpl chatClientImpl;
	private static ChatServer chatServerImpl;
	private JTextField senhatf;
	private ORB orbServidor;
	private org.omg.CORBA.Object refServer;
	public ChatServer instanceChatserver;
	public boolean vemDoCadastrar;
	private String usuarioLocal;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			LoginGUI frame = new LoginGUI();
			frame.setVisible(true);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the frame.
	 */
	public LoginGUI() {
		setTitle("Tela Inicial");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 262, 236);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		vemDoCadastrar = false;

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		logintf = new JTextField();
		logintf.setBounds(55, 50, 129, 21);
		panel.add(logintf);
		logintf.setColumns(10);

		JButton btnOk = new JButton("Ok");
		btnOk.setBounds(99, 100, 53, 21);
		final LoginGUI _this = this;
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				usuarioLocal = logintf.getText();
				String senha = senhatf.getText();
				chatServerImpl = _this.resolveServidor();
				if (chatServerImpl.loginValido(usuarioLocal, senha)) {
					ChatClient chatC = chatServerImpl.bindUsuario(usuarioLocal,	vemDoCadastrar);
					ListaAmigosGUI zap = new ListaAmigosGUI();
					zap.setTitle(usuarioLocal);
					zap.setChatCliente(chatC);
					chatServerImpl.setStatusOnline(usuarioLocal);
					zap.setUsuarioLocal(usuarioLocal);
					if(!vemDoCadastrar){
						String[] text = chatServerImpl.getMessagemDaFila(usuarioLocal);
					}
					zap.montaTabelaInicial();
					//Fazer um for para atualizar as telas dos usuarios amigos
		        	//montaTabelaInicial();	 
					
					zap.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(null,
							"Usuário ou senha incorretos", "Nome",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panel.add(btnOk);

		JButton btnNewButton = new JButton("Cadastrar");
		btnNewButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				CadastroUsuario telaDeCadastro = new CadastroUsuario();
				telaDeCadastro.setTelaDeLogin(_this);
				_this.setVisible(false);
				telaDeCadastro.setVisible(true);
			}
		});
		btnNewButton.setBounds(70, 164, 106, 21);
		panel.add(btnNewButton);
		
		addWindowListener(new WindowAdapter() {  
	        @Override public void windowClosing(WindowEvent e) {  
	            //AÇÃO ANTES DE FECHAR  
	        	instanceChatserver = resolveServidor();
	        	instanceChatserver.desconectaUsuario(usuarioLocal);
	        	//Fazer um for para atualizar as telas dos usuarios
	        	//montaTabelaInicial();	        	
	        }  
	    });

		senhatf = new JTextField();
		senhatf.setColumns(10);
		senhatf.setBounds(55, 75, 129, 21);
		panel.add(senhatf);
	}

	public void setClientImpl(ChatClientImpl clientImpl) {
		this.chatClientImpl = clientImpl;
	}

	public ChatServer resolveServidor() {
		String args[] = new String[6];
		args[0] = "-ORBInitialHost";
		args[1] = "localhost";
		args[2] = "-ORBInitialPort";
		args[3] = "5000";
		args[4] = "-Djava.ext.dirs=/home/filipe/ppd/chat/openjms-0.7.7-beta-1/lib";
		orbServidor = ORB.init(args, null);
		try {
			org.omg.CORBA.Object obj = orbServidor
					.resolve_initial_references("NameService");
			NamingContext naming = NamingContextHelper.narrow(obj);
			NameComponent[] name = { new NameComponent("chatserver", "zapzap") };
			refServer = naming.resolve(name);
			instanceChatserver = ChatServerHelper.narrow(refServer);
			return instanceChatserver;
		} catch (Exception e) {
		}
		return null;
	}

	public void close() {
		this.hide();
	}

	public void setFlagVemDoCadastrar(boolean value) {
		this.vemDoCadastrar = value;
	}
	
	public void setUsuarioLogin(String user){
		this.usuarioLocal = user;
	}
}
