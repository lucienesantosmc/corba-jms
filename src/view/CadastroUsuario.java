package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;

import org.ppd.zapzap.ChatClientImpl;
import org.ppd.zapzap.ChatServerImpl;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CadastroUsuario extends JFrame {

	private JPanel contentPane;
	private JTextField tfNomeUsuario;
	private JTextField tfSenhaUsuario;
	private static ChatClientImpl chatClientImpl;
	private static ChatServerImpl chatServerImpl;
	private LoginGUI telaDeLogin;

	/**
	 * Create the frame.
	 */
	public CadastroUsuario() {
		setTitle("Cadastro de Usuário");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 261, 198);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		tfNomeUsuario = new JTextField();
		tfNomeUsuario.setFont(new Font("Dialog", Font.PLAIN, 13));
		tfNomeUsuario.setBounds(85, 35, 141, 19);
		contentPane.add(tfNomeUsuario);
		tfNomeUsuario.setColumns(10);
		
		
		JButton btnNewButton = new JButton("Salvar");
		final CadastroUsuario _this = this; 
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String usuarioLocal = tfNomeUsuario.getText();
				String senha = tfSenhaUsuario.getText();
				
				ChatServerImpl chatServer = new ChatServerImpl();
				
				if(chatServer.validaNomeDoUsuario(usuarioLocal)){
					chatClientImpl =  new ChatClientImpl(usuarioLocal);
					chatClientImpl.cadastrarUsuario(usuarioLocal, senha);
					_this.getTelaDeLogin().setUsuarioLogin(usuarioLocal);
				//	_this.getTelaDeLogin().setChatClientImpl(chatClientImpl);
					_this.getTelaDeLogin().setFlagVemDoCadastrar(true);
					_this.getTelaDeLogin().setVisible(true);
					_this.getTelaDeLogin().setClientImpl(chatClientImpl);
					JOptionPane.showMessageDialog(null, "Cliente cadastrado com sucesso.", "Nome", JOptionPane.INFORMATION_MESSAGE);
					_this.setVisible(false);
					
				}else{
					JOptionPane.showMessageDialog(null, "Cliente já cadastrado", "Nome", JOptionPane.ERROR_MESSAGE);
					System.out.println("Cliente Já cadastrado!");
				}
			}
		});
		btnNewButton.setBounds(139, 132, 86, 25);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("Nome:");
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 13));
		lblNewLabel.setBounds(27, 34, 51, 21);
		contentPane.add(lblNewLabel);
		
		JLabel lblSenha = new JLabel("Senha:");
		lblSenha.setFont(new Font("Dialog", Font.BOLD, 13));
		lblSenha.setBounds(27, 67, 70, 21);
		contentPane.add(lblSenha);
		
		tfSenhaUsuario = new JTextField();
		tfSenhaUsuario.setBounds(85, 68, 141, 19);
		contentPane.add(tfSenhaUsuario);
		tfSenhaUsuario.setColumns(10);
	}

	public LoginGUI getTelaDeLogin() {
		return telaDeLogin;
	}

	public void setTelaDeLogin(LoginGUI telaDeLogin) {
		this.telaDeLogin = telaDeLogin;
	}
	
	
}
