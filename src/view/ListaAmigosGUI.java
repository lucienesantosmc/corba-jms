package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.ppd.zapzap.ChatClientImpl;

import chat.ChatClient;
import chat.ChatServer;
import chat.ChatServerHelper;

public class ListaAmigosGUI extends JFrame {

	private JPanel contentPane;
	private JTextField nickAmigotf;
	private ChatClientImpl chatClientImpl;
	private ChatClient chatCliente;
	private ChatServer instanceChatserver;
	private JTable tblAmigos;
	private BatePapoGUI batePapo;
	public String usuarioLocal;
	private	String[] amigos;

	
	private ORB orbServidor;
	private org.omg.CORBA.Object refServer;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListaAmigosGUI frame = new ListaAmigosGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ListaAmigosGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 250, 399);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblContatos = new JLabel("Contatos");
		lblContatos.setBounds(12, 12, 70, 15);
		panel.add(lblContatos);
		
		nickAmigotf = new JTextField();
		nickAmigotf.setBounds(12, 328, 130, 19);
		panel.add(nickAmigotf);
		nickAmigotf.setColumns(10);
		
		JButton btnNewButton = new JButton("Add");
		final ListaAmigosGUI _this = this;
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String novoAmigo = nickAmigotf.getText();
				if(_this.getChatCliente().verificaSeUsuarioAmigoExiste(novoAmigo)){
					amigos = _this.getChatCliente().adicionarAmigo(novoAmigo);
					montarTabelaDeAmigos(tblAmigos, amigos);
					nickAmigotf.setText("");
				}
				else{
					JOptionPane.showMessageDialog(_this, "Usuário não encontrado", "Adicionando amigo", JOptionPane.INFORMATION_MESSAGE);
				 }
			}
		});
		
		addWindowListener(new WindowAdapter() {  
	        @Override public void windowClosing(WindowEvent e) {  
	            //AÇÃO ANTES DE FECHAR  
	        	instanceChatserver = resolveServidor();
	        	instanceChatserver.desconectaUsuario(usuarioLocal);
	        	//Fazer um for para atualizar as telas dos usuarios
	        	//montaTabelaInicial();	        	
	        }  
	    });
		
		btnNewButton.setBounds(154, 325, 65, 25);
		panel.add(btnNewButton);
		
		tblAmigos = new JTable();
		tblAmigos.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getClickCount() == 2) {
					int row = tblAmigos.getSelectedRow();
					String amigo = (String) tblAmigos.getModel().getValueAt(row, 0);
				    String amigoArray[] = new String[3];  
					amigoArray = amigo.split(" - "); 
					String amigoSelecionado = amigoArray[0];
					
					batePapo = new BatePapoGUI();
					batePapo.setTitle(usuarioLocal);
					batePapo.setAmigoBatePapo(amigoSelecionado);
					batePapo.setUsuarioLocal(usuarioLocal);
					chatClientImpl = new ChatClientImpl(usuarioLocal);
					chatClientImpl.setChatCliente(chatCliente);
					chatClientImpl.updateAllAmigos(amigos);
					chatClientImpl.addTelaDeBatePapo(batePapo, amigoSelecionado);
					//chatClientImpl.setTelaBatePapo(batePapo, amigoSelecionado);
					batePapo.setChatClientImpl(chatClientImpl);					
					batePapo.setVisible(true);
				}
			}
		});
		tblAmigos.setModel(new DefaultTableModel(
	        new Object[][] {
	        },
	        new String[] {
	        }
		));
		tblAmigos.setName("tblAmigos");		
		tblAmigos.setBounds(12, 39, 207, 274);
		panel.add(tblAmigos);
				
	}
	
	public ChatServer resolveServidor() {
		String args[] = new String[6];
		args[0] = "-ORBInitialHost";
		args[1] = "localhost";
		args[2] = "-ORBInitialPort";
		args[3] = "5000";
		args[4] = "-Djava.ext.dirs=/home/filipe/ppd/chat/openjms-0.7.7-beta-1/lib";
		orbServidor = ORB.init(args, null);
		try {
			org.omg.CORBA.Object obj = orbServidor
					.resolve_initial_references("NameService");
			NamingContext naming = NamingContextHelper.narrow(obj);
			NameComponent[] name = { new NameComponent("chatserver", "zapzap") };
			refServer = naming.resolve(name);
			instanceChatserver = ChatServerHelper.narrow(refServer);
			return instanceChatserver;
		} catch (Exception e) {
		}
		return null;
	}
	
	
	public void montaTabelaInicial(){
		
		int count = chatCliente.getQuantAmigosDoUsuario();
		if(count > 0){		
			montarTabelaDeAmigos(tblAmigos, chatCliente.formataStringAmigos());
		}
	}
	
	public void montarTabelaDeAmigos(JTable tblAmigos, String amigos[]){
		DefaultTableModel model = new DefaultTableModel(new Object[]{"amigos"}, 0);
		for(String amigo: amigos){
			Vector<Object> row = new Vector<>();
			row.add(amigo );
			model.addRow(row);
		}
		tblAmigos.setModel(model);
	}
	
	public ChatClientImpl getChatClientImpl() {
		return chatClientImpl;
	}

	public void setChatClientImpl(ChatClientImpl chatClientImpl) {
		this.chatClientImpl = chatClientImpl;
	}

	public String getUsuarioLocal() {
		return usuarioLocal;
	}

	public void setUsuarioLocal(String usuarioLocal) {
		this.usuarioLocal = usuarioLocal;
	}

	public ChatClient getChatCliente() {
		return chatCliente;
	}

	public void setChatCliente(ChatClient chatCliente) {
		this.chatCliente = chatCliente;
	}
	
}
