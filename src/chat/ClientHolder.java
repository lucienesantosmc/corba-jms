package chat;

/**
* chat/ClientHolder.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from chat.idl
* Saturday, April 19, 2014 11:03:06 AM BRT
*/

public final class ClientHolder implements org.omg.CORBA.portable.Streamable
{
  public chat.Client value = null;

  public ClientHolder ()
  {
  }

  public ClientHolder (chat.Client initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = chat.ClientHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    chat.ClientHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return chat.ClientHelper.type ();
  }

}
