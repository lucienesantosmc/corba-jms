package org.ppd.zapzap;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

public class Server {
	public static void main(String args[]) {
		try{
			
			String arguments[] = new String[6];
			arguments[0] = "-ORBInitialHost";
			arguments[1] = "localhost";
			arguments[2] = "-ORBInitialPort";
			arguments[3] = "5000";			
			arguments[4] = "-Djava.ext.dirs=/home/filipe/ppd/chat/openjms-0.7.7-beta-1/lib";
			
			// initializing ORB
			ORB orb = ORB.init(arguments,null);

			// getting reference to POA
			org.omg.CORBA.Object objPoa = orb.resolve_initial_references("RootPOA");
			POA rootpoa = POAHelper.narrow(objPoa);
			
			// getting NameService
			org.omg.CORBA.Object obj = orb.resolve_initial_references("NameService");
			
			//NamingContextExt ncRef = org.omg.CosNaming.NamingContextExtHelper.narrow(obj);
			
			NamingContext ncRef = org.omg.CosNaming.NamingContextHelper.narrow(obj);
			
			// creating servant
			ChatServerImpl cs = new ChatServerImpl();
		
			org.omg.CORBA.Object  ojbRef = rootpoa.servant_to_reference(cs);
			
			NameComponent[] name = {new NameComponent("chatserver", "zapzap")};
			ncRef.rebind(name, ojbRef);
			rootpoa.the_POAManager().activate();
			
			//ChatServer chatserver = cs._this(orb);
			// binding servant reference to NameService
			//ncRef.rebind(ncRef.to_name("chatserver"), chatserver);

			System.out.println("Servidor ativado");
			// starting orb
			orb.run();

		} catch(Exception e) {
			e.printStackTrace(System.out);
		}
	}
}



