package org.ppd.zapzap;

import java.util.HashMap;
import java.util.Map;

import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

import view.BatePapoGUI;
import chat.ChatClient;
import chat.ChatClientHelper;
import chat.ChatServer;
import chat.ChatServerHelper;


public class ChatClientImpl extends chat.ChatClientPOA {
	
	//Campos
	private String usuarioLocal;
	private String senha;
	//private HashMap<String, chat.ChatClient> amigos;
	private String[] amigosStr;
	private String statusUsuario;
	private ChatClient chatCliente;
		
	
	private Usuario usuario;
	//private String[] amigosFormatado;
	
	private Map<String, BatePapoGUI> telas;
	
	private org.omg.CORBA.Object refServer;
	private org.omg.CORBA.Object objRef1;
	public ChatServer instanceChatserver;
	ORB orbServidor;
	ORB orbCliente;
	org.omg.CORBA.Object obj1;
	NamingContext naming1;
	POA rootPOA;
	String args[] = null;
	
	public void config(){
		args = new String[6];
		args[0] = "-ORBInitialHost";
		args[1] = "localhost";
		args[2] = "-ORBInitialPort";
		args[3] = "5000";			
		args[4] = "-Djava.ext.dirs=/home/filipe/ppd/chat/openjms-0.7.7-beta-1/lib";
	}
		
	public ChatClientImpl(String nick){
		this.usuario = new Usuario();
		
		this.usuario.setNome(nick);
		this.usuarioLocal = nick;
		telas = new HashMap<String, BatePapoGUI>(); 
		this.usuario.setQuantAmigos(0);
		String[] listAmigos = new String[50];
		for (int i = 0; i < 50; i++) {
			listAmigos[i] = "";
		}
		this.usuario.setAmigosFormatado(listAmigos);
		
		senha = "";
		
		config();		
	
		try {
			orbServidor = ORB.init(args, null);
			orbCliente = ORB.init(args, null);
			org.omg.CORBA.Object obj = orbServidor.resolve_initial_references("NameService");
			NamingContext naming = NamingContextHelper.narrow(obj);
			NameComponent[] name = { new NameComponent("chatserver", "zapzap") };
			refServer = naming.resolve(name);
			instanceChatserver = ChatServerHelper.narrow(refServer);		
		}catch(Exception e){
		}	
	}
		
	public void cadastrarUsuario(String usuarioLocal, String senha){
		try {
			org.omg.CORBA.Object objPoa = orbCliente.resolve_initial_references("RootPOA");
		    rootPOA = POAHelper.narrow(objPoa);
			obj1 = orbCliente.resolve_initial_references("NameService");
			naming1 = NamingContextHelper.narrow(obj1);
			NameComponent[] nameCliente = { new NameComponent(usuarioLocal, "zapzap") }; //Registra  cliente (como Servidor) para outros terem acesso a ele pelo nome
			objRef1 = rootPOA.servant_to_reference(this);			
			naming1.rebind(nameCliente, objRef1);
			rootPOA.the_POAManager().activate();			
			ChatClientImpl impl = new ChatClientImpl(usuarioLocal);
			impl.setSenha(senha);
			ChatClient cc = impl._this(orbCliente);				
			instanceChatserver.cadastrar(usuarioLocal, cc);
			
		} catch(Exception e) {
			e.printStackTrace(System.out);
		}
	}
	
	public boolean verificaSeUsuarioAmigoExiste(String nickAmigo){
		return instanceChatserver.amigoExiste(nickAmigo);
	}
	
	public String[] montaUsuarioEstatus(Usuario usuario){
		return null;
	}
	//Adiciona o orb do amigo se ele estiver online, senão só adiciona o nome dele na lista de nomes de amigos.
	public String[] adicionarAmigo(String nickAmigo) {
		try {
			ChatClient chatDoAmigo = instanceChatserver.getChatClienteAmigoOnline(nickAmigo);
			
			if(chatDoAmigo != null){
				usuario.getListaChatClienteAmigos().put(nickAmigo, chatDoAmigo);
				usuario.getAmigosFormatado()[usuario.getQuantAmigos()] = nickAmigo + " - " + "Online";
			}else{
				usuario.getAmigosFormatado()[usuario.getQuantAmigos()] = nickAmigo + " - " + "Offline";
			}
			usuario.getListaNomesAmigos()[usuario.getQuantAmigos()] = nickAmigo;
			usuario.setQuantAmigos(usuario.getQuantAmigos() + 1);
			
		}catch(Exception e){
		}
		instanceChatserver.atualizaUsuario(usuario.getNome(), usuario.getQuantAmigos(), usuario.getListaNomesAmigos(), usuario.getAmigosFormatado());
		return usuario.getAmigosFormatado();
   }
	
	public void enviarMessagem(String remetente, String destinatario, String mensagem) {
		
		if( usuario.getStatus()!= null && usuario.getStatus().equals("Online") ){
			ChatClient chatDoAmigo = instanceChatserver.getChatCliente(destinatario);
			chatDoAmigo.atualiza(remetente, mensagem);
			this.atualiza(destinatario, mensagem);
		}else{
			instanceChatserver.colocarNafilaJms(destinatario, remetente,  mensagem);
		}
	}
	
	public String[] formataStringAmigos(){
		
		String[] list = usuario.getListaNomesAmigos();
		//Verifica o status
		for(int i=0; i< usuario.getQuantAmigos(); i++){
			if(instanceChatserver.getChatClienteAmigoOnline(list[i]) != null){
				this.usuario.getAmigosFormatado()[i] = list[i] + " - Online";
			}else{
				this.usuario.getAmigosFormatado()[i] = list[i] + " - Offline";
			}
		}
		return this.usuario.getAmigosFormatado();
	}
	
	@Override
	public void atualizaStatusDoAmigo(String usuarioLogando, String status) {
		
		ChatClient chatDoAmigo = instanceChatserver.getChatClienteAmigoOnline(usuarioLogando);
		
		String[] list = usuario.getAmigosFormatado();
		for(int i=0; i< usuario.getQuantAmigos(); i++){
			if(list[i].equals(usuarioLogando)){
				usuario.getAmigosFormatado()[i] = usuarioLogando + " - " +status;
				break;
			}
		}
		if(status.equals("Online")){
			if(usuario.getListaChatClienteAmigos().get(usuarioLogando) != null){
				usuario.getListaChatClienteAmigos().remove(usuarioLogando);
			}
			usuario.getListaChatClienteAmigos().put(usuarioLogando, chatDoAmigo);
		}
	}
	
	
	public void atualiza(String nick, String text) {
		
		BatePapoGUI telaRemetente = this.getTelaBatePapo(nick);
		if(telaRemetente != null){
			String historico  = this.getTelaBatePapo(nick).getTaMessage().getText();
			historico = historico + "\n" + text;
			this.getTelaBatePapo(nick).getTaMessage().setText(historico);
			this.getTelaBatePapo(nick).getTfEnviarMessage().setText("");
		}else{
			this.adicionarAmigo(nick);
			BatePapoGUI tela = new BatePapoGUI();
			tela.getTaMessage().setText(text);
			tela.getTfEnviarMessage().setText("");
			tela.setAmigoBatePapo(nick);
			tela.getLbAmigoBatePapo().setText(nick);
			tela.setChatClientImpl(this);
			tela.setUsuarioLocal(this.usuarioLocal);			
			this.addTelaDeBatePapo(tela, nick);
			tela.setVisible(true);
		}		
	}

	public BatePapoGUI getTelaBatePapo(String amigo) {
		return telas.get(amigo);		
	}

	public void setTelaBatePapo(BatePapoGUI telaBatePapo, String amigo) {
		for(Map.Entry<String, BatePapoGUI> entry : telas.entrySet()){
			if(entry.getKey() == amigo){
				telas.put(amigo, telaBatePapo);
			}
		}
	}

	public void updateAllAmigos(String[] stringAmigos){
		
		String args[] = new String[6];
		args[0] = "-ORBInitialHost";
		args[1] = "localhost";
		args[2] = "-ORBInitialPort";
		args[3] = "5000";			
		args[4] = "-Djava.ext.dirs=/home/filipe/ppd/chat/openjms-0.7.7-beta-1/lib";
			
		try {
			org.omg.CORBA.Object obj;
			obj = orbServidor.resolve_initial_references("NameService");
			NamingContext naming = NamingContextHelper.narrow(obj);
			//Procura a referência do amigo
			ChatClient instanceChatClient;
			for(String amigoAux: stringAmigos){
			    String amigoArray[] = new String[3];  
				amigoArray = amigoAux.split(" - "); 
				String amigo = amigoArray[0];
				if(!amigo.equals("")){
					NameComponent[] name = { new NameComponent(amigo, "zapzap") };
					refServer = naming.resolve(name);
					instanceChatClient = ChatClientHelper.narrow(refServer);
					usuario.getListaChatClienteAmigos().put(amigo, instanceChatClient);
				 }
			  }
		} catch (InvalidName | NotFound | CannotProceed	| org.omg.CosNaming.NamingContextPackage.InvalidName e) {
			e.printStackTrace();
		} 
	}
	
	public void addTelaDeBatePapo(BatePapoGUI batePapo, String amigoSelecionado) {
		telas.put(amigoSelecionado, batePapo);
	}


	public String getSenha() {
		return senha;
	}


	public void setSenha(String senha) {
		this.senha = senha;
	}


	public String getStatusUsuario() {
		return statusUsuario;
	}

	public ChatServer getInstanceChatserver() {
		return instanceChatserver;
	}


	public void setInstanceChatserver(ChatServer instanceChatserver) {
		this.instanceChatserver = instanceChatserver;
	}


	public ChatClient getChatCliente() {
		return chatCliente;
	}

	public void setChatCliente(ChatClient chatCliente) {
		this.chatCliente = chatCliente;
	}

	public void setNomeDoUsuario(String nomeUsuario){
		this.usuario.setNome(nomeUsuario);
		
	}
	public void setStatusDoUsuario(String status){
		this.usuario.setStatus(status);
	}
	public void setListaNomesAmigos(String[] amigos){
		this.usuario.setListaNomesAmigos(amigos);
	}
	public String[]	getListaNomesAmigos(){
		return this.usuario.getListaNomesAmigos();
	}

	public int getQuantAmigosDoUsuario(){
		return this.usuario.getQuantAmigos();
	}
	
	public void setQuantAmigosDoUsuario(int qnt){
		this.usuario.setQuantAmigos(qnt);
	}
}

