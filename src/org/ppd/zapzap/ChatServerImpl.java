package org.ppd.zapzap;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.exolab.castor.jdo.oql.NoMoreTokensException;
import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

import chat.ChatClient;
import chat.ChatServer;
import chat.ChatServerHelper;
import chat.Client;
import chat.UnknownID;
public class ChatServerImpl extends chat.ChatServerPOA  {
	
	public Map<String, chat.ChatClient> todos = new HashMap<String, chat.ChatClient>();
	public List<String> nicks = new Vector<String>();	
	public Map<String, Usuario> listUsuarios = new HashMap<String, Usuario>();
	
	private org.omg.CORBA.Object refServer;
	private org.omg.CORBA.Object objRef1;
	public ChatServer instanceChatserver;
	ORB orbServidor;
	ORB orbCliente;
	org.omg.CORBA.Object obj1;
	NamingContext naming1;
	POA rootPOA;
	String[] args = new String[6];
		
	
	public boolean validaNomeDoUsuario(String novoUsuario) {
		if (nicks.contains(novoUsuario))
			return false;
		return true;
	}
	
	private void config(){
	   	args[0] = "-ORBInitialHost";
		args[1] = "localhost";
		args[2] = "-ORBInitialPort";
		args[3] = "5000";			
		args[4] = "-Djava.ext.dirs=/home/filipe/ppd/chat/openjms-0.7.7-beta-1/lib";
		
	}
	
	public void desconectaUsuario(String usuarioSaindo){
		listUsuarios.get(usuarioSaindo).setStatus("Offline");
		setStatusOffline(usuarioSaindo);
//		if(todos.get(usuarioSaindo) != null){
//			todos.remove(usuarioSaindo);
//			todos.put(usuarioSaindo, null);
//		}
		
	}
	
	public void setStatusOffline(String usuarioSaindo) {
		ChatClient chatClienteAmigo = todos.get(usuarioSaindo);
		String[] amigosTemp = chatClienteAmigo.getListaNomesAmigos();		
		for(String amigo : amigosTemp){
			if(!amigo.equals("")){
			  chatClienteAmigo = todos.get(amigo);
			  chatClienteAmigo.atualizaStatusDoAmigo(usuarioSaindo, "Offline");
			}
		}
	}
	
	//Registra um novo orb para o cliente cadastrado que ficou online agora
   public ChatClient bindUsuario(String usuarioLogando, boolean vemDoCadastrar){
		
	    if(!vemDoCadastrar){
		    config();
		  
			orbServidor = ORB.init(args, null);
			orbCliente = ORB.init(args, null);
			ChatClient cc = null;
			try {
				org.omg.CORBA.Object obj = orbServidor.resolve_initial_references("NameService");
				NamingContext naming = NamingContextHelper.narrow(obj);
				NameComponent[] name = { new NameComponent("chatserver", "zapzap") };
				refServer = naming.resolve(name);
				instanceChatserver = ChatServerHelper.narrow(refServer);		
				org.omg.CORBA.Object objPoa = orbCliente.resolve_initial_references("RootPOA");
			    rootPOA = POAHelper.narrow(objPoa);
				obj1 = orbCliente.resolve_initial_references("NameService");
				naming1 = NamingContextHelper.narrow(obj1);
				NameComponent[] nameCliente = { new NameComponent(usuarioLogando, "zapzap") }; //Registra  cliente (como Servidor) para outros terem acesso a ele pelo nome
				objRef1 = rootPOA.servant_to_reference(this);			
				naming1.rebind(nameCliente, objRef1);
				rootPOA.the_POAManager().activate();
				
				ChatClientImpl impl = new ChatClientImpl(usuarioLogando);
				cc = impl._this(orbCliente);	
				atualizaChatClienteNaLista(cc, usuarioLogando);
				
				return cc;
				
			} catch(Exception e) {
				e.printStackTrace(System.out);
			}
			return cc;
		}else{
			return todos.get(usuarioLogando);
		}
	}
   
   //Ao se logar o usuario precisa atualizar o orb dele na lista de chatClientes(todos) para prover comunicação. Visto que o rob que la se encontra n possue mais comunicação.
   	private void atualizaChatClienteNaLista(ChatClient cc, String usuarioLogando){
		
		cc.setStatusDoUsuario("Online");
		cc.setNomeDoUsuario(usuarioLogando);
		cc.setQuantAmigosDoUsuario(listUsuarios.get(usuarioLogando).getQuantAmigos());
		if(listUsuarios.get(usuarioLogando) != null &&  listUsuarios.get(usuarioLogando).getListaNomesAmigos() != null ){
			cc.setListaNomesAmigos(listUsuarios.get(usuarioLogando).getListaNomesAmigos());
		}else{
			String[] amigos = new String[50];
			for (int i = 0; i < 50; i++) {
				amigos[i] = "";
			}
			cc.setListaNomesAmigos(amigos);
		}
		if(todos.get(usuarioLogando) != null){
			todos.remove(usuarioLogando);
		}
		todos.put(usuarioLogando, cc);
   }
	
    public void atualizaUsuario(String usuarioCurrent, int qntAmigos, String[] listaNomesAmigos, String[] amigosFormatado){
    	listUsuarios.get(usuarioCurrent).setQuantAmigos(qntAmigos);
    	listUsuarios.get(usuarioCurrent).setStatus("Online");
    	listUsuarios.get(usuarioCurrent).setListaNomesAmigos(listaNomesAmigos);
    	listUsuarios.get(usuarioCurrent).setAmigosFormatado(amigosFormatado);
    }
	
    public void cadastrar(String nick, chat.ChatClient clientNovo) {
				
		Usuario novoUsuario = new Usuario();
		novoUsuario.setNome(nick);
		novoUsuario.setSenha(clientNovo.getSenha());
		novoUsuario.setStatus("Online");
		
		nicks.add(nick);
		todos.put(nick, clientNovo);
		listUsuarios.put(nick, novoUsuario);
		System.out.println("cadastrado: " + nick);
	}
	
	public boolean amigoExiste(String nomeAmigo){
		if(todos.get(nomeAmigo) != null) return true;
		return false;
	}
	
	public ChatClient getChatClienteAmigoOnline(String amigo){
		if(listUsuarios.get(amigo).getStatus().equals("Online")){
			return todos.get(amigo);
		}
		return null;
	}
	@Override
	public void setStatusOnline(String usuarioLogado) {
		
		ChatClient chatClienteAmigo = todos.get(usuarioLogado);		
		String[] amigosTemp = chatClienteAmigo.getListaNomesAmigos();
		for(String amigo : amigosTemp){
			if(!amigo.equals("")){
				chatClienteAmigo = todos.get(amigo);
				chatClienteAmigo.atualizaStatusDoAmigo(usuarioLogado, "Online");
			}
		}
	}
	
	public boolean loginValido(String usuario, String senha){
		
		Usuario usuarioLogando = listUsuarios.get(usuario);
		if(usuarioLogando != null && usuarioLogando.getSenha().equals(senha)){
			return true;
		}
		return false;
	}
	
	public ChatClient getChatCliente(String usuario){
		return todos.get(usuario);
	}
	
	public String[] getMessagemDaFila(String nick) {
		try{
			
			Context context = initContext();
			QueueConnectionFactory qfactory = (QueueConnectionFactory) context.lookup("ConnectionFactory");
			QueueConnection qconnection = qfactory.createQueueConnection();
			QueueSession qsession = qconnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			qconnection.start();
		        Queue dest = (Queue)context.lookup(nick);
	        	QueueReceiver qreceiver = qsession.createReceiver(dest);
	        	TextMessage textMessage = (TextMessage) qreceiver.receive();
	
			if(textMessage != null){
				 System.out.println(" Mensagem Recebida: " + textMessage.getText()); 
				 String[] arrayText = textMessage.getText().split("\n");
				 return arrayText;
			}
	     }catch(Exception e){
	             e.printStackTrace();
	     }
		return null;       
      }     
	
	public void descadastrar(String nick) throws chat.UnknownID {
//		System.out.println("descadastrar: " + nick);
//		//Client c = todos.remove(nick);
//		if (c == null) throw new chat.UnknownID();
//		nicks.remove(c.nick);
	}

	@Override
	public void enviarMensagem(String id, String text) throws UnknownID {
	}

	public Context initContext() throws NamingException{
		Hashtable properties = new Hashtable();
		properties.put(Context.INITIAL_CONTEXT_FACTORY,"org.exolab.jms.jndi.InitialContextFactory");
		properties.put(Context.PROVIDER_URL, "tcp://localhost:3035/");

		return new InitialContext(properties);
	}
	@Override
	public void colocarNafilaJms(String nick, String remetente, String messagem) {
		try{

			Context context = initContext();

			QueueConnectionFactory qfactory = (QueueConnectionFactory) context.lookup("ConnectionFactory");

			QueueConnection qconnection = qfactory.createQueueConnection();
			QueueSession qsession = qconnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

			TextMessage message = qsession.createTextMessage();
			message.setText(messagem);
			
			Queue queue = qsession.createQueue(nick); 
			
			context.rebind(nick, queue);   			
			
			Queue dest = (Queue) context.lookup(nick);
			QueueSender sender = qsession.createSender(dest);
			sender.send(message);
	        
			context.close();
			qconnection.close();
			
		}catch(Exception e){
	      e.printStackTrace();
	    }
	 }

	
}